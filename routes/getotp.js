const express = require('express');
const router = express.Router();

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

// Sendmail settings
const sendmail = require('sendmail')();

// If you need to configure your mail server settings,
// you can configure sendmail below blocked comments.

/* 
const sendmail = require('sendmail')({
  logger: {
    debug: console.log,
    info: console.info,
    warn: console.warn,
    error: console.error
  },
  silent: false,
  dkim: { // Default: False
    privateKey: fs.readFileSync('./dkim-private.pem', 'utf8'),
    keySelector: 'mydomainkey'
  },
  devPort: 1025, // Default: False
  devHost: 'localhost', // Default: localhost
  smtpPort: 2525, // Default: 25
  smtpHost: 'localhost' // Default: -1 - extra smtp host after resolveMX
})
*/

//Models
const UserObject = require('../models/User');

// Get OTP
router.post('/', (req, res, next) => {
  const {
    email
  } = req.body;

  // Generate an OTP
  const num = Math.floor(Math.random() * (999999 - 100000)) + 100000;
  const otp = num.toString();

  // Get hash and save to the database.
  bcrypt.hash(otp, 10).then((hash) => {
    const promise = UserObject.updateOne({
      email
    }, {
      email: email,
      otp: hash
    }, {
      upsert: true
    });
    promise.then((data) => {
     
      console.log("otp:" + otp);
      //Send e-mail
      const promise = sendmail({
        from: 'no-reply@yourdomain.com',
        to: 'test@test.com',
        subject: otp + ' is your one time password.',
        html: 'Dear user. This is your one time password:' + otp
      }, function (err, reply) {
        console.log(err && err.stack);
        console.dir(reply);
      });
      promise.then((result) => {
        if (!result) {
          // Prepare a token. 
          const payload = {
            email
          };
          const token = jwt.sign(payload, process.env.TEMPORARILYTOKENKEY, {
            expiresIn: 900 // This token expires 15 minutes later.  
          });

          // Send a response.
          res.status(200).json({
            success: true,
            message: "If your address is correct, you will receive an email!",
            token: token,
            //otp: otp
            // The above line is added for the route test. Uncomment it, when testing.
          });
        } else {
          res.status(500).json({
            success: false,
            message: "Something went wrong! Plese try again later!"
          });
        }
      }).catch((err) => {
        console.log(err);
        res.status(500).json({
          success: false,
          message: "Something went wrong! Plese try again later!"
        });
      });
    }).catch((err) => {
      console.log(err);
      res.status(500).json({
        success: false,
        message: "Something went wrong. Try again later."
      });
    });
  });

});

module.exports = router;